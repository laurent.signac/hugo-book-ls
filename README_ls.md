Intégration des modifs de la branche officielle

// git remote add upstream https://github.com/alex-shpak/hugo-book
git fetch upstream
git rebase upstream/master / plutôt git merge upstream/master
git push origin master --force


# 1
Modif dans : 
thems/assets/_defaults.scss

```
$mobile-breakpoint: $menu-width + $body-min-width * 1.8 + $toc-width !default;
```

Autres interventions :

- assets
  - _custom.scss
  - _defaults.scss
  - _variables.scss
- layouts
  - sitemap.xml
  - partials/docs/inject/body.html
  - partials/docs/inject/head.html
  - shortcodes/katex.html
- ~~static~~
  - ~~syntax-dracula.css~~
  - ~~syntax-code.css~~
  - ~~syntax-code-monokai.css~~
  - ~~sjcl.min.js~~
  - ~~challenges.js~~



# 2

PB recherche qui marche pas...
assets/search-data.json
Ligne supprimée : 
{{- $pages = where $pages "Content" "not in" (slice nil "") -}}

**Ce pb a été réglé dans un commit ultérieur dans upstream**
